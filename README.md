# Retail Emailing
Balíček pro podporu odesílání emailů přes Retail Emailing

## Požadavky
- verze PHP 5.6 a vyšší
- balíček nette/nette ~2.4
- Backend je třeba před implementací upravit pro podporu dynamického přidávání nových modulů. Zajišťuje to balíček ,,redenge/redenge". Implementováno na Gumex, JRC, A3sport (všude, kde se dělalo EET), zde je tedy možné načerpat inspiraci jak na to.

## Instalace
Nejlepší možnost, jak nainstalovat balíček je přes composer  [Composer](http://getcomposer.org/):

1) Do svého composer.json přidejte odkaz na privátní repozitář satis
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```

2) Stáhněte balíček a doinstalujte závilosti:
```sh
$ composer require redenge/retailemailing
$ composer install
```

3) Do config.neon (pro frontend) přidat extension:
```twig
extensions:
	retailEmailing: Redenge\RetailEmailing\FrontModule\DI\RetailEmailingExtension
```

4) Do config.neon (pro backend) přidat extension:
```twig
extensions:
	- Redenge\RetailEmailing\AdminModule\DI\RetailEmailingExtension
```

5) Do souboru engine/etc/modules.php přidat následující pole:
```php
[
	'name' => 'RetailEmailing',
	'caption' => 'RetailEmailing',
	'extension' => ':RetailEmailing:Config:default',
]
```
- to zajistí zobrazení modulu v administraci

6) Syntaxe pro konfiguraci nastavení, kampaní, atd. pro Retail Emailing
```yml
app_url: ''
app_username: ''
app_password: ''

settings:
	-
		environment: Redenge\RetailEmailing\FrontModule\EnvironmentKeyFactory::create(<kod multishopu>, <kod zeme>)
		# environment: Redenge\RetailEmailing\FrontModule\EnvironmentKeyFactory::create('gumex', 'cz')

		# Definice kampaní pro prostředí environment výše
		campaigns:
			- Redenge\RetailEmailing\FrontModule\Entity\CampaignFactory::create(<id kampane>, <kod kampane>)
			#- Redenge\RetailEmailing\FrontModule\Entity\CampaignFactory::create(4, 'activation')

```

## Použití
```php
use Redenge\RetailEmailing\FrontModule\AppFactory;
use Redenge\RetailEmailing\FrontModule\EnvironmentKeyFactory;

class MyPresenter extends Nette\Application\UI\Presenter
{

	/** @var AppFactory @inject */
	public $retailEmailingFactory;


    public function process()
    {
		$appRetail = $this->retailEmailingFactory->create(
			EnvironmentKeyFactory::create($this->environment->multishopCode, $this->environment->countryIso));
		$params = [
			'recipients' => array(
					array (
						"email" => 'novak@redenge.cz'
				   )
			),
			"properties" => array(
				"lostpassword" => 'foo',
			)
		];
		$appRetail->sendCampaign('activation', $params);

    }
}
```