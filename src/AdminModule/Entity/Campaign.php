<?php

namespace Redenge\RetailEmailing\AdminModule\Entity;

use Redenge\Engine\Entity\Entity;


/**
 * Description of Campaign
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Campaign extends Entity
{


	private $code;

	private $multishop;

	private $country;


	public function getCode()
	{
		return $this->code;
	}


	public function getMultishop()
	{
		return $this->multishop;
	}


	public function getCountry()
	{
		return $this->country;
	}


	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}


	public function setMultishop($multishop)
	{
		$this->multishop = $multishop;
		return $this;
	}


	public function setCountry($country)
	{
		$this->country = $country;
		return $this;
	}


}
