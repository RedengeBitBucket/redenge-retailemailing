<?php

namespace Redenge\RetailEmailing\AdminModule\Entity;

use Redenge\Engine\Entity\Entity;
use Redenge\Engine\Entity\ISettings;


/**
 * Description of Settings
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Settings extends Entity implements ISettings
{

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $setting;

	/**
	 * @var int
	 */
	private $idSettingsTheme;


	public function getCode()
	{
		return $this->code;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getType()
	{
		return $this->type;
	}


	public function getSetting()
	{
		return $this->setting;
	}


	public function getIdSettingsTheme()
	{
		return $this->idSettingsTheme;
	}


	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}


	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}


	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}


	public function setSetting($setting)
	{
		$this->setting = $setting;
		return $this;
	}


	public function setIdSettingsTheme($idSettingsTheme)
	{
		$this->idSettingsTheme = $idSettingsTheme;
		return $this;
	}

}
