<?php

namespace Redenge\RetailEmailing\AdminModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;


class RetailEmailingExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['RetailEmailing' => 'Redenge\RetailEmailing\AdminModule\Presenters\*Presenter']]
		);
	}

}
