<?php

namespace Redenge\RetailEmailing\FrontModule;


/**
 * Description of EnvironmentKeyFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EnvironmentKeyFactory
{

	/**
	 * @param string $multishopCode
	 * @param string $countryCode
	 *
	 * @return EnvironmentKey
	 */
	public static function create($multishopCode, $countryCode)
	{
		return new EnvironmentKey($multishopCode, $countryCode);
	}

}
