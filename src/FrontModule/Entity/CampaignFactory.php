<?php

namespace Redenge\RetailEmailing\FrontModule\Entity;


/**
 * Description of CampaignFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CampaignFactory
{

	/**
	 * @param int $campaignId
	 * @param string $campaignCode
	 *
	 * @return Campaign
	 */
	public static function create($campaignId, $campaignCode)
	{
		return new Campaign($campaignId, $campaignCode);
	}

}
