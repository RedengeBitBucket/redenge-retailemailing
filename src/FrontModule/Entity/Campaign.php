<?php

namespace Redenge\RetailEmailing\FrontModule\Entity;


/**
 * Description of Campaign
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Campaign
{

	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $code;


	/**
	 * @param int $id
	 * @param string $code
	 */
	public function __construct($id, $code)
	{
		$this->id = $id;
		$this->code = $code;
	}

}
