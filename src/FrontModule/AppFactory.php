<?php

namespace Redenge\RetailEmailing\FrontModule;

use Redenge\RetailEmailing\FrontModule\ProviderContainer;


/**
 * Description of AppFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class AppFactory
{

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var ProviderContainer
	 */
	private $providerContainer;


	public function __construct(Settings $settings, ProviderContainer $providerContainer)
	{
		$this->settings = $settings;
		$this->providerContainer = $providerContainer;
	}


	/**
	 * @return ProviderContainer
	 */
	public function getProviderContainer()
	{
		return $this->providerContainer;
	}


	public function create(EnvironmentKey $environmentKey)
	{
		return new App($this->settings, $this->providerContainer->getProvider($environmentKey));
	}
}
