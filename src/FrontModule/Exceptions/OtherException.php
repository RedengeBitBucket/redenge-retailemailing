<?php

namespace Redenge\RetailEmailing\FrontModule\Exceptions;


/**
 * Description of OtherExeption
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class OtherException extends RetailEmailingException
{

}
