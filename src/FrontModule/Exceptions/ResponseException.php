<?php

namespace Redenge\RetailEmailing\FrontModule\Exceptions;

use Redenge\RetailEmailing\FrontModule\Http\Response;


/**
 * Description of ResponseException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ResponseException extends RetailEmailingException
{

	/**
	 * @var Response The response that threw the exception.
	 */
	protected $response;

	/**
	 * @var array Decoded response.
	 */
	protected $responseData;


	/**
	 *
	 * @param Response $response
	 * @param ApiRedengeException $previousException
	 */
	public function __construct(Response $response, RetailEmailingException $previousException = null)
	{
		$this->response = $response;
		$this->responseData = $response->getDecodedBody();

		$errorMessage = $this->get('Message', 'Unknown error from API RetailEmailing.');
		$errorCode = $this->get('Code', -1);
		$errorCode = !is_numeric($errorCode) ? 0 : $errorCode;

		parent::__construct($errorMessage, $errorCode, $previousException);
	}


	/**
	 * A factory for creating the appropriate exception based on the response from API Redenge.
	 *
	 * @param Response $response The response that threw the exception.
	 *
	 * @return ResponseException
	 */
	public static function create(Response $response)
	{
		$statusCode = $response->getHttpStatusCode();
		$code = null;
		$message = 'Unknown error from API RetailEmailing.';

		if ($statusCode === 401) {
			$message = 'Nepodařilo se připojit na vzdálené API. Vzdálený API server je nedostupný nebo jsou nesprávné'
				. ' přihlašovací údaje';
			return new static($response, new AuthenticationException($message, 401));
		} elseif ($statusCode === 500) {
			$message = '';
			$code = 500;
			return new static($response, new OtherException($message, $code));
		}

		return new static($response, new OtherException($message, $code));
	}


	/**
	 * Checks isset and returns that or a default value.
	 *
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	private function get($key, $default = null)
	{
		if (isset($this->responseData[$key])) {
			return $this->responseData[$key];
		}

		return $default;
	}


	/**
	 * Returns the HTTP status code
	 *
	 * @return int
	 */
	public function getHttpStatusCode()
	{
		return $this->response->getHttpStatusCode();
	}


	/**
	 * Returns the sub-error code
	 *
	 * @return int
	 */
	public function getSubErrorCode()
	{
		return $this->get('error_subcode', -1);
	}


	/**
	 * Returns the error type
	 *
	 * @return string
	 */
	public function getErrorType()
	{
		return $this->get('type', '');
	}


	/**
	 * Returns the raw response used to create the exception.
	 *
	 * @return string
	 */
	public function getRawResponse()
	{
		return $this->response->getBody();
	}


	/**
	 * Returns the decoded response used to create the exception.
	 *
	 * @return array
	 */
	public function getResponseData()
	{
		return $this->responseData;
	}


	/**
	 * Returns the response entity used to create the exception.
	 *
	 * @return FacebookResponse
	 */
	public function getResponse()
	{
		return $this->response;
	}

}
