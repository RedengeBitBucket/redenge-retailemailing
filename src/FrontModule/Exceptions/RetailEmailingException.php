<?php

namespace Redenge\RetailEmailing\FrontModule\Exceptions;

use Exception;


/**
 * Description of RetailEmailingException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RetailEmailingException extends Exception
{

}
