<?php

namespace Redenge\RetailEmailing\FrontModule\Exceptions;


/**
 * Description of AuthorizationException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class AuthenticationException extends RetailEmailingException
{

}
