<?php

namespace Redenge\RetailEmailing\FrontModule;

use OutOfBoundsException;
use Redenge\RetailEmailing\FrontModule\Entity\Campaign;


class Provider
{

	/**
	 * @var EnvironmentKey
	 */
	private $environmentKey;

	/**
	 * @var Campaign[]
	 */
	private $campaigns;


	public function __construct(EnvironmentKey $environmentKey, array $campaigns)
	{
		$this->environmentKey = $environmentKey;
		$this->campaigns = $campaigns;
	}


	/**
	 * @param EnvironmentKey $key
	 *
	 * @return bool
	 */
	public function matchKey(EnvironmentKey $environmentKey)
	{
		return (string) $environmentKey === (string) $this->environmentKey;
	}


	/**
	 * @param string $campaignCode
	 * @return Campaign
	 * @throws OutOfBoundsException
	 */
	public function getCampaign($campaignCode)
	{
		foreach ($this->campaigns as $campaign) {
			if ($campaign->code === $campaignCode) {
				return $campaign;
			}
		}

		throw new OutOfBoundsException("Campaign ,,$campaignCode'' not exists");
	}

}
