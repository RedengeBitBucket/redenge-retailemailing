<?php

namespace Redenge\RetailEmailing\FrontModule;

use Redenge\RetailEmailing\FrontModule\Http\Request;
use Redenge\RetailEmailing\FrontModule\Http\Response;
use Redenge\RetailEmailing\FrontModule\HttpClients\HttpClientInterface;


/**
 * Description of Client
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Client
{

	/**
	 * @const int The timeout in seconds for a normal request.
	 */
	const DEFAULT_REQUEST_TIMEOUT = 300;

	/**
	 * @var HttpClientInterface HTTP client handler.
	 */
	protected $httpClientHandler;


	public function __construct(HttpClientInterface $httpClientHandler)
	{
		$this->httpClientHandler = $httpClientHandler;
	}


	/**
	 * Sets the HTTP client handler.
	 *
	 * @param HttpClientInterface $httpClientHandler
	 */
	public function setHttpClientHandler(HttpClientInterface $httpClientHandler)
	{
		$this->httpClientHandler = $httpClientHandler;
	}


	/**
	 * Returns the HTTP client handler.
	 *
	 * @return HttpClientInterface
	 */
	public function getHttpClientHandler()
	{
		return $this->httpClientHandler;
	}


	/**
	 * Prepares the request for sending to the client handler.
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function prepareRequestMessage(Request $request)
	{
		$url = $request->getUrl();

		$request->setHeaders([
			'Content-Type' => 'application/json',
		]);

		return [
			$url,
			$request->getMethod(),
			$request->getHeaders(),
			$request->getParams(),
		];
	}


	/**
	 * Makes the request to Graph and returns the result.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	public function sendRequest(Request $request)
	{
		list($url, $method, $headers, $body) = $this->prepareRequestMessage($request);

		$timeOut = static::DEFAULT_REQUEST_TIMEOUT;

		$rawResponse = $this->httpClientHandler->send($url, $method, $body, $headers, $timeOut);

		$returnResponse = new Response(
			$rawResponse->getBody(), $rawResponse->getHttpResponseCode(), $rawResponse->getHeaders()
		);

		if ($returnResponse->isError()) {
			throw $returnResponse->getThrownException();
		}

		return $returnResponse;
	}

}
