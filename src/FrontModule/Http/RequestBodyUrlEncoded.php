<?php

namespace Redenge\RetailEmailing\FrontModule\Http;


/**
 * Description of RequestBodyUrlEncoded
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
final class RequestBodyUrlEncoded implements RequestBodyInterface
{

	/**
	 * @var array The parameters to send with this request.
	 */
	protected $params = [];


	/**
	 * Creates a new UrlEncodedBody entity.
	 *
	 * @param array $params
	 */
	public function __construct(array $params)
	{
		$this->params = $params;
	}


	/**
	 * {@inheritdoc}
	 */
	public function getBody()
	{
		return http_build_query($this->params, null, '&');
	}

}
