<?php

namespace Redenge\RetailEmailing\FrontModule\Http;

use Redenge\RetailEmailing\FrontModule\App;
use Redenge\OnlineUser\FrontModule\Tools\UrlHelper;


/**
 * Description of Request
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Request
{

	/**
	 * @var App entity.
	 */
	protected $app;

	/**
	 * @var string The HTTP method for this request.
	 */
	protected $method;

	/**
	 * @var string The endpoint for this request.
	 */
	protected $endpoint;

	/**
	 * @var array The headers to send with this request.
	 */
	protected $headers = [];

	/**
	 * @var array The parameters to send with this request.
	 */
	protected $params = [];


	/**
	 * Creates a new Request entity.
	 *
	 * @param App|null        $app
	 * @param string|null             $method
	 * @param string|null             $endpoint
	 * @param array|null              $params
	 */
	public function __construct(App $app = null, $method = null, $endpoint = null, array $params = [])
	{
		$this->setApp($app);
		$this->setMethod($method);
		$this->setEndpoint($endpoint);
		$this->setParams($params);
	}


	/**
	 * @return App
	 */
	public function getApp()
	{
		return $this->app;
	}


	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}


	/**
	 * @return string
	 */
	public function getEndpoint()
	{
		return $this->endpoint;
	}


	/**
	 * @return array
	 */
	public function getHeaders()
	{
		$headers = $this->getDefaultHeaders();

		return array_merge($this->headers, $headers);
	}


	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}


	 /**
     * Only return params on POST requests.
     *
     * @return array
     */
    public function getPostParams()
    {
        if ($this->getMethod() === 'POST') {
            return $this->getParams();
        }

        return [];
    }


	/**
	 * @param \Redenge\RetailEmailing\FrontModule\App $app
	 */
	public function setApp(App $app)
	{
		$this->app = $app;
	}


	/**
	 * @param string $method
	 */
	public function setMethod($method)
	{
		$this->method = strtoupper($method);
	}


	/**
	 * @param string $endpoint
	 */
	public function setEndpoint($endpoint)
	{
		$this->endpoint = $endpoint;
	}


	/**
	 * @param array $headers
	 */
	public function setHeaders(array $headers)
	{
		$this->headers = array_merge($this->headers, $headers);
	}


	/**
	 * @param array $params
	 */
	public function setParams(array $params)
	{
		$this->params = array_merge($this->params, $params);
	}


	/**
	 * Generate and return the URL for this request.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		$this->validateMethod();

		$baseUrl = $this->app->getSettings()->getAppUrl();
		$url = $baseUrl . UrlHelper::forceSlashPrefix($this->getEndpoint());

		if (!in_array($this->getMethod(), ['POST'])) {
			$params = $this->getParams();
			$url = UrlHelper::appendParamsToUrl($url, $params);
		}

		return $url;
	}


	/**
	 * Validate that the HTTP method is set.
	 *
	 * @throws ApiRedengeException
	 */
	private function validateMethod()
	{
		if (!$this->method) {
			throw new Exception('HTTP method not specified.');
		}

		if (!in_array($this->method, ['GET', 'POST'])) {
			throw new Exception('Invalid HTTP method specified.');
		}
	}


	/**
	 * Return the default headers that every request should use.
	 *
	 * @return array
	 */
	private function getDefaultHeaders()
	{
		$credentials = base64_encode(sprintf('%s:%s', $this->app->getSettings()->getAppUsername(), $this->app->getSettings()->getAppPassword()));

		return [
			'Authorization' => 'Basic ' . $credentials,
		];
	}

}
