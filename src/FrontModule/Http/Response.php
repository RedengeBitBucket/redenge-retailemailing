<?php

namespace Redenge\RetailEmailing\FrontModule\Http;

use Redenge\RetailEmailing\FrontModule\Exceptions\ResponseException;


/**
 * Description of Response
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Response
{

	/**
	 * @var int The HTTP status code response.
	 */
	protected $httpStatusCode;

	/**
	 * @var array
	 */
	protected $headers;

	/**
	 * @var string The raw body of the response.
	 */
	protected $body;

	/**
	 * @var array The decoded body of the response.
	 */
	protected $decodedBody = [];

	/**
	 * @var Request The original request that returned this response.
	 */
	protected $request;

	/**
	 * @var ApiRedengeException The exception thrown by this request.
	 */
	protected $thrownException;


	/**
	 * Creates a new Response entity.
	 *
	 * @param string|null     $body
	 * @param int|null        $httpStatusCode
	 * @param array|null      $headers
	 */
	public function __construct($body = null, $httpStatusCode = null, array $headers = [])
	{
		$this->body = $body;
		$this->httpStatusCode = $httpStatusCode;
		$this->headers = $headers;

		$this->decodeBody();
	}


	/**
	 * Return the HTTP status code for this response.
	 *
	 * @return int
	 */
	public function getHttpStatusCode()
	{
		return $this->httpStatusCode;
	}


	/**
	 * Return the HTTP headers for this response.
	 *
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->headers;
	}


	/**
	 * Return the raw body response.
	 *
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}


	/**
	 * Return the decoded body response.
	 *
	 * @return array
	 */
	public function getDecodedBody()
	{
		return $this->decodedBody;
	}


	/**
	 * Returns true if API returned an error message.
	 *
	 * @return bool
	 */
	public function isError()
	{
		return $this->httpStatusCode !== 200 || (isset($this->decodedBody['state']) && $this->decodedBody['state'] !== 'ok');
	}


	/**
	 * Throws the exception.
	 *
	 * @throws ApiRedengeException
	 */
	public function throwException()
	{
		throw $this->thrownException;
	}


	/**
	 * Instantiates an exception to be thrown later.
	 */
	public function makeException()
	{
		$this->thrownException = ResponseException::create($this);
	}


	/**
	 * Returns the exception that was thrown for this request.
	 *
	 * @return ResponseException|null
	 */
	public function getThrownException()
	{
		return $this->thrownException;
	}


	public function decodeBody()
	{
		$this->decodedBody = json_decode($this->body, true);
		if ($this->decodedBody === null) {
			$this->decodedBody = [];
			parse_str($this->body, $this->decodedBody);
		}

		if (!is_array($this->decodedBody)) {
			$this->decodedBody = [];
		}

		if ($this->isError()) {
			$this->makeException();
		}
	}

}
