<?php

namespace Redenge\RetailEmailing\FrontModule\Http;


/**
 * Description of RequestBodyInterface
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface RequestBodyInterface
{

	/**
	 * Get the body of the request to API
	 *
	 * @return string
	 */
	public function getBody();

}
