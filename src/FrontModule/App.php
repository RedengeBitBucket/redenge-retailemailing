<?php

namespace Redenge\RetailEmailing\FrontModule;

use Redenge\RetailEmailing\FrontModule\Http\Request;
use Redenge\RetailEmailing\FrontModule\Http\Response;
use Redenge\RetailEmailing\FrontModule\HttpClients\GuzzleHttpClient;


/**
 * Description of App
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class App
{

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var Provider
	 */
	private $provider;

	/**
	 * @var Client
	 */
	private $client;


	public function __construct(Settings $settings, Provider $provider)
	{
		$this->settings = $settings;
		$this->provider = $provider;
		$this->client = new Client(new GuzzleHttpClient);
	}


	/**
	 * @return Settings
	 */
	public function getSettings()
	{
		return $this->settings;
	}


	public function setProvider(Provider $provider)
	{
		$this->provider = $provider;
	}


	/**
	 * Sends a POST request and returns the result.
	 *
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	private function post($endpoint, array $params = [])
	{
		return $this->sendRequest('POST', $endpoint, $params);
	}


	/**
	 * Sends a request and returns the result.
	 *
	 * @param string                  $method
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	private function sendRequest($method, $endpoint, array $params = [])
	{
		$request = $this->request($method, $endpoint, $params);

		return $this->client->sendRequest($request);
	}


	/**
	 * Instantiates a new Request entity.
	 *
	 * @param string                  $method
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Request
	 *
	 * @throws ApiRedengeException
	 */
	public function request($method, $endpoint, array $params = [])
	{
		return new Request(
			$this, $method, $endpoint, $params
		);
	}


	/**
	 * @param string $campaignCode
	 * @param array $params
	 */
	public function sendCampaign($campaignCode, array $params)
	{
		$campaign = $this->provider->getCampaign($campaignCode);

		$this->post(sprintf('/campaigns/%d/sendemaildynamic', $campaign->id), $params);
	}

}
