<?php

namespace Redenge\RetailEmailing\FrontModule;


class Settings
{

	/**
	 * @var string
	 */
	private $appUrl;

	/**
	 * @var string
	 */
	private $appUsername;

	/**
	 * @var string
	 */
	private $appPassword;


	/**
	 * @param string $appUrl
	 * @param string $appUsername
	 * @param string $appPassword
	 */
	public function __construct($appUrl, $appUsername, $appPassword)
	{
		$this->appUrl = $appUrl;
		$this->appUsername = $appUsername;
		$this->appPassword = $appPassword;
	}


	/**
	 * @return string
	 */
	public function getAppUrl()
	{
		return $this->appUrl;
	}


	/**
	 * @return string
	 */
	public function getAppUsername()
	{
		return $this->appUsername;
	}


	/**
	 * @return string
	 */
	public function getAppPassword()
	{
		return $this->appPassword;
	}

}
