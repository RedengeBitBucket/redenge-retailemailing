<?php

namespace Redenge\RetailEmailing\Tools;


/**
 * Description of UrlHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class UrlHelper
{

	/**
	 * Check for a "/" prefix and prepend it if not exists.
	 *
	 * @param string|null $string
	 *
	 * @return string|null
	 */
	public static function forceSlashPrefix($string)
	{
		if (!$string) {
			return $string;
		}

		return strpos($string, '/') === 0 ? $string : '/' . $string;
	}


	/**
	 * Returns the params from a URL in the form of an array.
	 *
	 * @param string $url The URL to parse the params from.
	 *
	 * @return array
	 */
	public static function getParamsAsArray($url)
	{
		$query = parse_url($url, PHP_URL_QUERY);
		if (!$query) {
			return [];
		}
		$params = [];
		parse_str($query, $params);

		return $params;
	}


	/**
	 * Gracefully appends params to the URL.
	 *
	 * @param string $url       The URL that will receive the params.
	 * @param array  $newParams The params to append to the URL.
	 *
	 * @return string
	 */
	public static function appendParamsToUrl($url, array $newParams = [])
	{
		if (empty($newParams)) {
			return $url;
		}

		if (strpos($url, '?') === false) {
			return $url . '?' . http_build_query($newParams, null, '&');
		}

		list($path, $query) = explode('?', $url, 2);
		$existingParams = [];
		parse_str($query, $existingParams);

		// Favor params from the original URL over $newParams
		$newParams = array_merge($newParams, $existingParams);

		// Sort for a predicable order
		ksort($newParams);

		return $path . '?' . http_build_query($newParams, null, '&');
	}

}
