<?php

namespace Redenge\RetailEmailing\FrontModule\HttpClients;


/**
 * Description of HttpClientInterface
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface HttpClientInterface
{

	/**
	 * Sends a request to the server and returns the raw response.
	 *
	 * @param string $url     The endpoint to send the request to.
	 * @param string $method  The request method.
	 * @param array $body    The body of the request.
	 * @param array  $headers The request headers.
	 * @param int    $timeOut The timeout in seconds for the request.
	 *
	 * @return \Redenge\OnlineUser\Http\RawResponse Raw response from the server.
	 *
	 * @throws \Redenge\OnlineUser\Exceptions\ApiRedengeException
	 */
	public function send($url, $method, array $body, array $headers, $timeOut);

}
