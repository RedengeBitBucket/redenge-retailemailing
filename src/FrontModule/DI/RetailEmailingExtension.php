<?php

namespace Redenge\RetailEmailing\FrontModule\DI;

use Nette\DI\CompilerExtension;
use Nette\DI\Statement;
use Nette\Utils\Validators;
use Redenge\RetailEmailing\FrontModule\AppFactory;
use Redenge\RetailEmailing\FrontModule\EnvironmentKey;
use Redenge\RetailEmailing\FrontModule\Provider;
use Redenge\RetailEmailing\FrontModule\ProviderContainer;
use Redenge\RetailEmailing\FrontModule\Settings;


class RetailEmailingExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	public $defaults = [
		'app_url' => '',
		'app_username' => '',
		'app_password' => '',
	];


	/**
	 * return void
	 */
	public function loadConfiguration()
	{
		$config = $this->getConfig();

		$defaults = array_diff_key($this->defaults, $config);
		foreach ($defaults as $key => $val) {
			$config[$key] = $this->defaults[$key];
		}

		Validators::assertField($config, 'app_url');
		Validators::assertField($config, 'app_username');
		Validators::assertField($config, 'app_password');

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('settings'))
			->setClass(Settings::class, [
				'appUrl' => $config['app_url'],
				'appUsername' => $config['app_username'],
				'appPassword' => $config['app_password'],
		]);

		$providers = [];
		foreach ($config['settings'] as $settings) {
			$campaigns = [];
			if (count($settings['campaigns'])) {
				foreach ($settings['campaigns'] as $campaign) {
					$campaigns[] = $campaign;
				}
			}

			$providers[] = new Statement(Provider::class, [
				'environmentKey' => (!($environment = $settings['environment']) instanceof Statement) ? 
					new Statement(EnvironmentKey::class, [$environment]) : $environment,
				'campaigns' => $campaigns
			]);

		}

		$builder->addDefinition($this->prefix('providerContainer'))
			->setClass(ProviderContainer::class)
			->setArguments([
				'providers' => $providers,
			]);

		$builder->addDefinition($this->prefix('appFactory'))
			->setClass(AppFactory::class, [$this->prefix('@settings'), $this->prefix('@providerContainer')]);
	}

}
